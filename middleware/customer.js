export default function ({ store, redirect }) {
  if (store.getters.isCustomer !== true) {
    return redirect('/')
  }
}
