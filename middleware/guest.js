export default function ({ store, redirect }) {
  if (store.getters.isGuest !== true) {
    return redirect('/')
  }
}
