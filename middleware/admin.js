export default function ({ store, redirect }) {
  if (store.getters.isAdmin !== true) {
    return redirect('/')
  }
}
