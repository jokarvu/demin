import Vue from 'vue'
import firebase from 'firebase/app'

import 'firebase/auth'
import 'firebase/storage'
import 'firebase/firestore'
import 'firebase/functions'

const firebaseConfig = {
  apiKey: 'AIzaSyDXe3v66cB8SCOrnB_S9KdJt-7A0QRDCV4',
  authDomain: 'inbooker.firebaseapp.com',
  databaseURL: 'https://inbooker.firebaseio.com',
  projectId: 'inbooker',
  storageBucket: 'inbooker.appspot.com',
  messagingSenderId: '199495858641',
  appId: '1:199495858641:web:e7c1e65b674cc809'
}

firebase.initializeApp(firebaseConfig)

Vue.prototype.$fstore = firebase.firestore()
Vue.prototype.$fauth = firebase.auth()
Vue.prototype.$fstorage = firebase.storage()
Vue.prototype.$ffun = firebase.functions()
Vue.prototype.$FSTORE = firebase.firestore

// get counter
Vue.prototype.$loadCounter = function (docId) {
  firebase.firestore().collection('counters').doc(docId).onSnapshot((doc) => {
    const counter = doc.data()
    this.counter = counter
  })
}

export const fstore = firebase.firestore()
export const fauth = firebase.auth()
export const fstorage = firebase.storage()
export const ffun = firebase.functions()
export const FAUTH = firebase.auth
export const FSTORE = firebase.firestore
