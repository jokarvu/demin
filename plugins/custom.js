import Vue from 'vue'
import Snotify, { SnotifyPosition } from 'vue-snotify'
import VeeValidate from 'vee-validate'
require('bootstrap')
// Snotify
const SnotifyOption = {
  toast: {
    position: SnotifyPosition.rightBottom
  }
}
Vue.use(Snotify, SnotifyOption)

Vue.filter('currency', function (value) {
  let tmp = value.toFixed(1).replace(/\d(?=(\d{3})+\.)/g, '$&,')
  tmp = tmp.split('.')[0]
  tmp = tmp.split(',').join('.')
  return tmp
})

Vue.filter('capitalize', function (value) {
  return value.charAt(0).toUpperCase() + value.slice(1)
})

Vue.filter('serverTimestamp', function (value) {
  return value.toDate().toLocaleDateString()
})

Vue.use(VeeValidate, {
  fieldsBagName: 'formFields'
})
