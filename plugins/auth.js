import { fauth, fstore } from '@/plugins/firebase'

export default (context) => {
  const { store } = context
  return new Promise((resolve, reject) => {
    fauth.onAuthStateChanged(async (user) => {
      let data
      if (user) {
        const userDoc = await fstore.collection('users').doc(user.uid).get()
        const userProfile = userDoc.data()
        data = {
          userId: user.uid,
          userProfile: userDoc.data(),
          isAuthenticated: true,
          isGuest: false,
          isAdmin: userProfile.role === 'admin',
          isCustomer: userProfile.role === 'customer'
        }
      } else {
        data = {
          userId: null,
          userProfile: null,
          isAuthenticated: false,
          isGuest: true,
          isAdmin: false,
          isCustomer: false
        }
      }
      store.dispatch('authenticate', data)
      resolve()
    })
  })
}
