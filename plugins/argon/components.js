import Vue from 'vue'
import BaseInput from '@/components/argon/BaseInput'
import NavbarToggleButton from '@/components/argon/NavbarToggleButton'
import BaseDropdown from '@/components/argon/BaseDropdown'
import BaseNav from '@/components/argon/BaseNav'
import BaseHeader from '@/components/argon/BaseHeader'
import StatsCard from '@/components/argon/StatsCard'
import BaseCheckbox from '@/components/argon/BaseCheckbox'
import BaseButton from '@/components/argon/BaseButton'
// import Modal from '@/components/argon/Modal'

Vue.component('base-input', BaseInput)
Vue.component('base-header', BaseHeader)
Vue.component('stats-card', StatsCard)
Vue.component('NavbarToggleButton', NavbarToggleButton)
Vue.component('BaseDropdown', BaseDropdown)
Vue.component('BaseNav', BaseNav)
Vue.component('base-checkbox', BaseCheckbox)
Vue.component('base-button', BaseButton)
// Vue.component('modal', Modal)
