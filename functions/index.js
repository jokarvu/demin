const functions = require('firebase-functions')
const admin = require('firebase-admin')

const serviceAccount = require('./inbooker-firebase-adminsdk-4y4d8-9fa259dc52.json')

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://inbooker.firebaseio.com'
})

exports.onUserCreate = functions.firestore.document('users/{userId}').onCreate((snap, context) => {
    const user = snap.data()
    const userId = context.params.userId
    const batch = admin.firestore().batch()
    // Update admin user counter
    const adminRef = admin.firestore().collection('counters').doc('admin')
    batch.update(adminRef, { 'user.total': admin.firestore.FieldValue.increment(1) })
    batch.update(adminRef, { 'user.active': admin.firestore.FieldValue.increment(1) })
    if (user.role === 'customer') {
        batch.update(adminRef, { 'user.customer': admin.firestore.FieldValue.increment(1) })
    } else {
        batch.update(adminRef, { 'user.admin': admin.firestore.FieldValue.increment(1) })
    }
    // Create new counter for new user
    if (user.role === 'customer') {
        const userRef = admin.firestore().collection('counters').doc(userId)
        batch.set(userRef, {
            order: {
                total: 0,
                pending: 0,
                confirmed: 0,
                success: 0,
                shipping: 0,
                cancelled: 0
            },
            review: 0,
            money: 0,
            favorite: 0
        })
    }
    return batch.commit()
})

exports.onCategoryCreate = functions.firestore.document('categories/{categoryId}').onCreate((snap, context) => {
    const category = snap.data()
    const batch = admin.firestore().batch()
    const adminRef = admin.firestore().collection('counters').doc('admin')
    batch.update(adminRef, { category: admin.firestore.FieldValue.increment(1) })
    return batch.commit()
})

exports.onCategoryUpdate = functions.firestore.document('categories/{categoryId}').onUpdate((change, context) => {
    const batch = admin.firestore().batch()
    const before = change.before.data()
    const after = change.after.data()
    const totalDiff = after.product.total - before.product.total
    const instockDiff = after.product.instock - before.product.instock
    const soldDiff = after.product.sold - before.product.sold
    const adminRef = admin.firestore().collection('counters').doc('admin')
    batch.update(adminRef, {'product.total': admin.firestore.FieldValue.increment(totalDiff)})
    batch.update(adminRef, {'product.sold': admin.firestore.FieldValue.increment(soldDiff)})
    batch.update(adminRef, {'product.instock': admin.firestore.FieldValue.increment(instockDiff)})
    return batch.commit()
})

exports.onProductCreate = functions.firestore.document('products/{productId}').onCreate((snap, context) => {
    const batch = admin.firestore().batch()
    const product = snap.data()
    const categoryRef = admin.firestore().collection('categories').doc(product.category.id)
    batch.update(categoryRef, { 'product.total': admin.firestore.FieldValue.increment(product.total) })
    batch.update(categoryRef, { 'product.sold': admin.firestore.FieldValue.increment(product.sold) })
    batch.update(categoryRef, { 'product.instock': admin.firestore.FieldValue.increment(product.instock) })
    return batch.commit()
})

exports.onProductUpdate = functions.firestore.document('products/{productId}').onUpdate((change, context) => {
    const batch = admin.firestore().batch()
    const before = change.before.data()
    const after = change.after.data()
    if (before.category.id === after.category.id) {
        // Neu khong thay doi category
        const totalDiff = after.total - before.total
        const soldDiff = after.sold - before.sold
        const instockDiff = after.instock - before.instock
        const categoryRef = admin.firestore().collection('categories').doc(after.category.id)
        batch.update(categoryRef, { 'product.total': admin.firestore.FieldValue.increment(totalDiff) })
        batch.update(categoryRef, { 'product.instock': admin.firestore.FieldValue.increment(instockDiff) })
        batch.update(categoryRef, { 'product.sold': admin.firestore.FieldValue.increment(soldDiff) })
    } else {
        // Neu doi category
        const oldCategoryRef = admin.firestore().collection('categories').doc(before.category.id)
        batch.update(oldCategoryRef, { 'product.total': admin.firestore.FieldValue.increment(-before.total) })
        batch.update(oldCategoryRef, { 'product.sold': admin.firestore.FieldValue.increment(-before.sold) })
        batch.update(oldCategoryRef, { 'product.instock': admin.firestore.FieldValue.increment(-before.instock) })
        const newCategoryRef = admin.firestore().collection('categories').doc(after.category.id)
        batch.update(newCategoryRef, { 'product.instock': admin.firestore.FieldValue.increment(after.instock) })
        batch.update(newCategoryRef, { 'product.total': admin.firestore.FieldValue.increment(after.total) })
        batch.update(newCategoryRef, { 'product.sold': admin.firestore.FieldValue.increment(after.sold) })
    }
    return batch.commit()
})

exports.onOrderCreate = functions.firestore.document('orders/{orderId}').onCreate((snap, context) => {
    const order = snap.data()
    const batch = admin.firestore().batch()
    const adminRef = admin.firestore().collection('counters').doc('admin')
    batch.update(adminRef, { 'order.total': admin.firestore.FieldValue.increment(1) })
    batch.update(adminRef, { 'order.pending': admin.firestore.FieldValue.increment(1) })
    const userRef = admin.firestore().collection('counters').doc(order.user.id)
    batch.update(userRef, { 'order.total': admin.firestore.FieldValue.increment(1) })
    batch.update(userRef, { 'order.pending': admin.firestore.FieldValue.increment(1) })
    order.products.map(product => {
        const productRef = admin.firestore().collection('products').doc(product.id)
        batch.update(productRef, { sold: admin.firestore.FieldValue.increment(product.quantity) })
        batch.update(productRef, { instock: admin.firestore.FieldValue.increment(-product.quantity) })
        batch.update(productRef, { 'order.total': admin.firestore.FieldValue.increment(1) })
        batch.update(productRef, { 'order.pending': admin.firestore.FieldValue.increment(1) })
        return 'ok'
    });
    return batch.commit()
})

exports.onOrderUpdate = functions.firestore.document('orders/{orderId}').onUpdate((change, context) => {
    const before = change.before.data()
    const after = change.after.data()
    const batch = admin.firestore().batch()
    const adminRef = admin.firestore().collection('counters').doc('admin')
    const userRef = admin.firestore().collection('counters').doc(before.user.id)
    // update before
    if (before.status === 'pending') {
        batch.update(adminRef, { 'order.pending': admin.firestore.FieldValue.increment(-1) })
        batch.update(userRef, { 'order.pending': admin.firestore.FieldValue.increment(-1) })
        before.products.map(product => {
            const productRef = admin.firestore().collection('products').doc(product.id)
            batch.update(productRef, { 'order.pending': admin.firestore.FieldValue.increment(-1) })
            return 'ok'
        });
    } else if (before.status === 'confirmed') {
        batch.update(adminRef, { 'order.confirmed': admin.firestore.FieldValue.increment(-1) })
        batch.update(userRef, { 'order.confirmed': admin.firestore.FieldValue.increment(-1) })
        before.products.map(product => {
            const productRef = admin.firestore().collection('products').doc(product.id)
            batch.update(productRef, { 'order.confirmed': admin.firestore.FieldValue.increment(-1) })
            return 'ok'
        });
    } else if (before.status === 'shipping') {
        batch.update(adminRef, { 'order.shipping': admin.firestore.FieldValue.increment(-1) })
        batch.update(userRef, { 'order.shipping': admin.firestore.FieldValue.increment(-1) })
        before.products.map(product => {
            const productRef = admin.firestore().collection('products').doc(product.id)
            batch.update(productRef, { 'order.shipping': admin.firestore.FieldValue.increment(-1) })
            return 'ok'
        });
    } else if (before.status === 'success') {
        batch.update(adminRef, { 'order.success': admin.firestore.FieldValue.increment(-1) })
        batch.update(adminRef, { money: admin.firestore.FieldValue.increment(-before.total) })
        batch.update(userRef, { 'order.success': admin.firestore.FieldValue.increment(-1) })
        batch.update(userRef, { money: admin.firestore.FieldValue.increment(-before.total) })
        before.products.map(product => {
            const productRef = admin.firestore().collection('products').doc(product.id)
            batch.update(productRef, { 'order.success': admin.firestore.FieldValue.increment(-1) })
            return 'ok'
        });
    } else {
        batch.update(adminRef, { 'order.cancelled': admin.firestore.FieldValue.increment(-1) })
        batch.update(userRef, { 'order.cancelled': admin.firestore.FieldValue.increment(-1) })
        before.products.map(product => {
            const productRef = admin.firestore().collection('products').doc(product.id)
            batch.update(productRef, { sold: admin.firestore.FieldValue.increment(product.quantity) })
            batch.update(productRef, { instock: admin.firestore.FieldValue.increment(-product.quantity) })
            batch.update(productRef, { 'order.cancelled': admin.firestore.FieldValue.increment(-1) })
            return 'ok'
        })
    }
    // update after
    if (after.status === 'pending') {
        batch.update(adminRef, { 'order.pending': admin.firestore.FieldValue.increment(1) })
        batch.update(userRef, { 'order.pending': admin.firestore.FieldValue.increment(1) })
        after.products.map(product => {
            const productRef = admin.firestore().collection('products').doc(product.id)
            batch.update(productRef, { 'order.pending': admin.firestore.FieldValue.increment(1) })
            return 'ok'
        });
    } else if (after.status === 'confirmed') {
        batch.update(adminRef, { 'order.confirmed': admin.firestore.FieldValue.increment(1) })
        batch.update(userRef, { 'order.confirmed': admin.firestore.FieldValue.increment(1) })
        after.products.map(product => {
            const productRef = admin.firestore().collection('products').doc(product.id)
            batch.update(productRef, { 'order.confirmed': admin.firestore.FieldValue.increment(1) })
            return 'ok'
        });
    } else if (after.status === 'shipping') {
        batch.update(adminRef, { 'order.shipping': admin.firestore.FieldValue.increment(1) })
        batch.update(userRef, { 'order.shipping': admin.firestore.FieldValue.increment(1) })
        after.products.map(product => {
            const productRef = admin.firestore().collection('products').doc(product.id)
            batch.update(productRef, { 'order.shipping': admin.firestore.FieldValue.increment(1) })
            return 'ok'
        });
    } else if (after.status === 'success') {
        batch.update(adminRef, { 'order.success': admin.firestore.FieldValue.increment(1) })
        batch.update(adminRef, { money: admin.firestore.FieldValue.increment(after.total) })
        batch.update(userRef, { 'order.success': admin.firestore.FieldValue.increment(1) })
        batch.update(userRef, { money: admin.firestore.FieldValue.increment(after.total) })
        after.products.map(product => {
            const productRef = admin.firestore().collection('products').doc(product.id)
            batch.update(productRef, { 'order.success': admin.firestore.FieldValue.increment(1) })
            return 'ok'
        });
    } else {
        // Neu huy don hang thi cap nhat lai so luong cua product
        batch.update(adminRef, { 'order.cancelled': admin.firestore.FieldValue.increment(1) })
        batch.update(userRef, { 'order.cancelled': admin.firestore.FieldValue.increment(1) })
        after.products.map(product => {
            const productRef = admin.firestore().collection('products').doc(product.id)
            batch.update(productRef, { sold: admin.firestore.FieldValue.increment(-product.quantity) })
            batch.update(productRef, { instock: admin.firestore.FieldValue.increment(product.quantity) })
            batch.update(productRef, { 'order.cancelled': admin.firestore.FieldValue.increment(1) })
            return 'ok'
        })
    }
    return batch.commit()
})

exports.onOrderDelete = functions.firestore.document('orders/{orderId}').onDelete((snap, context) => {
    const order = snap.data()
    const batch = admin.firestore().batch()
    const adminRef = admin.firestore().collection('counters').doc('admin')
    const userRef = admin.firestore().collection('counters').doc(order.user.id)
    batch.update(adminRef, { 'order.total': admin.firestore.FieldValue.increment(-1) })
    batch.update(userRef, { 'order.total': admin.firestore.FieldValue.increment(-1) })
    order.products.map(product => {
        const productRef = admin.firestore().collection('products').doc(product.id)
        batch.update(productRef, { 'order.total': admin.firestore.FieldValue.increment(-1) })
        return 'ok'
    });
    if (order.status === 'cancelled') {
        batch.update(adminRef, { 'order.cancelled': admin.firestore.FieldValue.increment(-1) })
        batch.update(userRef, { 'order.cancelled': admin.firestore.FieldValue.increment(-1) })
        order.products.map(product => {
            const productRef = admin.firestore().collection('products').doc(product.id)
            batch.update(productRef, { 'order.cancelled': admin.firestore.FieldValue.increment(-1) })
            return 'ok'
        });
    } else if (order.status === 'pending') {
        batch.update(adminRef, { 'order.pending': admin.firestore.FieldValue.increment(-1) })
        batch.update(userRef, { 'order.pending': admin.firestore.FieldValue.increment(-1) })
        order.products.map(product => {
            const productRef = admin.firestore().collection('products').doc(product.id)
            batch.update(productRef, { 'order.pending': admin.firestore.FieldValue.increment(-1) })
            return 'ok'
        });
    } else if (order.status === 'confirmed') {
        batch.update(adminRef, { 'order.confirmed': admin.firestore.FieldValue.increment(-1) })
        batch.update(userRef, { 'order.confirmed': admin.firestore.FieldValue.increment(-1) })
        order.products.map(product => {
            const productRef = admin.firestore().collection('products').doc(product.id)
            batch.update(productRef, { 'order.confirmed': admin.firestore.FieldValue.increment(-1) })
            return 'ok'
        });
    } else if (order.status === 'shipping') {
        batch.update(adminRef, { 'order.shipping': admin.firestore.FieldValue.increment(-1) })
        batch.update(userRef, { 'order.shipping': admin.firestore.FieldValue.increment(-1) })
        order.products.map(product => {
            const productRef = admin.firestore().collection('products').doc(product.id)
            batch.update(productRef, { 'order.shipping': admin.firestore.FieldValue.increment(-1) })
            return 'ok'
        });
    } else {
        batch.update(adminRef, { 'order.success': admin.firestore.FieldValue.increment(-1) })        
        batch.update(userRef, { 'order.success': admin.firestore.FieldValue.increment(-1) })
        order.products.map(product => {
            const productRef = admin.firestore().collection('products').doc(product.id)
            batch.update(productRef, { 'order.success': admin.firestore.FieldValue.increment(-1) })
            return 'ok'
        });        
    }
    order.products.map(product => {
        const productRef = admin.firestore().collection('products').doc(product.id)
        batch.update(productRef, { sold: admin.firestore.FieldValue.increment(-product.quantity) })
        batch.update(productRef, { instock: admin.firestore.FieldValue.increment(product.quantity) })
        return 'ok'
    })
    if (order.status === 'success') {
        batch.update(adminRef, { money: admin.firestore.FieldValue.increment(-order.total) })
        batch.update(userRef, { money: admin.firestore.FieldValue.increment(-order.total) })
    }
    return batch.commit()
})

exports.onProductDelete = functions.firestore.document('products/{productId}').onDelete((snap, context) => {
    // chi product sold = 0 moi co the xoa
    const product = snap.data()
    const batch = admin.firestore().batch()
    const categoryRef = admin.firestore().collection('categories').doc(product.category.id)
    batch.update(categoryRef, { 'product.total': admin.firestore.FieldValue.increment(-product.total) })
    batch.update(categoryRef, { 'product.instock': admin.firestore.FieldValue.increment(-product.instock) })
    return batch.commit()
})

exports.onCategoryDelete = functions.firestore.document('categories/{categoryId}').onDelete((snap, context) => {
    // Chi category co so luong hang hoa bang 0 moi co the xoa (instock = sold = total = 0)
    const batch = admin.firestore().batch()
    const adminRef = admin.firestore().collection('counters').doc('admin')
    batch.update(adminRef, { category: admin.firestore.FieldValue.increment(-1) })
    return batch.commit()
})

exports.onReviewCreate = functions.firestore.document('reviews/{reviewId}').onCreate(async (snap, context) => {
    const review = snap.data()
    const batch = admin.firestore().batch()
    const productRef = admin.firestore().collection('products').doc(review.product.id)
    // update product rate & counter
    const product = await productRef.get()
    const newRateValue = (product.data().rate * product.data().review + review.rate)/(product.data().review + 1)
    batch.update(productRef, { review: admin.firestore.FieldValue.increment(1) })
    batch.update(productRef, { rate: newRateValue })
    const adminRef = admin.firestore().collection('counters').doc('admin')
    batch.update(adminRef, { review: admin.firestore.FieldValue.increment(1) })
    return batch.commit()
})

exports.onReviewUpdate = functions.firestore.document('reviews/{reviewId}').onUpdate(async (change, context) => {
    const before = change.before.data()
    const after = change.after.data()
    const batch = admin.firestore().batch()
    const productRef = admin.firestore().collection('products').doc(before.product.id)
    const product = await productRef.get()
    if (before.rate !== after.rate) {
        const newRateValue = (product.data().rate * product.data().review - before.rate + after.rate) / product.data().review
        batch.update(productRef, { rate: newRateValue })
    }
    return batch.commit()
})

exports.onReviewDelete = functions.firestore.document('reviews/{reviewId}').onDelete(async (snap, context) => {
    const review = snap.data()
    const batch = admin.firestore().batch()
    const productRef = admin.firestore().collection('products').doc(review.product.id)
    const product = await productRef.get()
    batch.update(productRef, { rate: admin.firestore.FieldValue.increment(-review.rate/product.data().review) })
    batch.update(productRef, { review: admin.firestore.FieldValue.increment(-1) })
    const adminRef = admin.firestore().collection('counters').doc('admin')
    batch.update(adminRef, { review: admin.firestore.FieldValue.increment(-1) })
    return batch.commit()
})

exports.onImportCreate = functions.firestore.document('imports/{importId}').onCreate((snap, context) => {
    const imp = snap.data()
    const batch = admin.firestore().batch()
    imp.products.map(product => {
        const productRef = admin.firestore().collection('products').doc(product.id)
        batch.update(productRef, { total: admin.firestore.FieldValue.increment(product.quantity) })
        batch.update(productRef, { instock: admin.firestore.FieldValue.increment(product.quantity) })
        return 'ok'
    })
    return batch.commit()
})

exports.onImportUpdate = functions.firestore.document('imports/{importId}').onUpdate((change, context) => {
    const before = change.before.data()
    const after = change.after.data()
    const batch = admin.firestore().batch()
    after.products.map(product => {
        const productRef = admin.firestore().collection('products').doc(product.id)
        const tmp = before.products.find(prod => {
            return product.id === prod.id
        })
        if (tmp) {
            // Neu thay doi so luong
            const quantityDiff = product.quantity - tmp.quantity
            batch.update(productRef, { total: admin.firestore.FieldValue.increment(quantityDiff) })
            batch.update(productRef, { instock: admin.firestore.FieldValue.increment(quantityDiff) })
        } else {
            // Neu cap nhat them product moi
            batch.update(productRef, { total: admin.firestore.FieldValue.increment(product.quantity) })
            batch.update(productRef, { instock: admin.firestore.FieldValue.increment(product.quantity) })
        }
        return 'ok'
    })
    before.products.map(product => {
        const productRef = admin.firestore().collection('products').doc(product.id)
        const tmp = after.products.find(prod => {
            return product.id === prod.id
        })
        if (!tmp) {
            // neu xoa product khoi import
            batch.update(productRef, { total: admin.firestore.FieldValue.increment(-product.quantity) })
            batch.update(productRef, { instock: admin.firestore.FieldValue.increment(-product.quantity) })
        }
        return 'ok'
    })
    return batch.commit()
})

exports.onImportDelete = functions.firestore.document('imports/{importId}').onDelete((snap, context) => {
    const batch = admin.firestore().batch()
    const imp = snap.data()
    imp.products.map((product) => {
        const productRef = admin.firestore().collection('products').doc(product.id)
        batch.update(productRef, { total: admin.firestore.FieldValue.increment(-product.quantity) })
        batch.update(productRef, { instock: admin.firestore.FieldValue.increment(-product.quantity) })
        return 'ok'
    })
    return batch.commit()
})

exports.onVoucherCreate = functions.firestore.document('vouchers/{voucherId}').onCreate((snap, context) => {
    const batch = admin.firestore().batch()
    const adminRef = admin.firestore().collection('counters').doc('admin')
    batch.update(adminRef, { voucher: admin.firestore.FieldValue.increment(1) })
    return batch.commit()
})

exports.onVoucherDelete = functions.firestore.document('vouchers/{voucherId}').onDelete((snap, context) => {
    const batch = admin.firestore().batch()
    const adminRef = admin.firestore().collection('counters').doc('admin')
    batch.update(adminRef, { voucher: admin.firestore.FieldValue.increment(-1) })
    return batch.commit()
})