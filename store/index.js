export const state = () => ({
  isGuest: true,
  isAuthenticated: false,
  isCustomer: false,
  isAdmin: false,
  userId: null,
  userProfile: null
})

export const mutations = {
  SET_IS_GUEST(state, payload) {
    state.isGuest = payload
  },
  SET_IS_AUTHENTICATED(state, payload) {
    state.isAuthenticated = payload
  },
  SET_IS_CUSTOMER(state, payload) {
    state.isCustomer = payload
  },
  SET_IS_ADMIN(state, payload) {
    state.isAdmin = payload
  },
  SET_USER_ID(state, payload) {
    state.userId = payload
  },
  SET_USER_PROFILE(state, payload) {
    state.userProfile = payload
  }
}

export const getters = {
  isGuest(state) {
    return state.isGuest
  },
  isAuthenticated(state) {
    return state.isAuthenticated
  },
  isCustomer(state) {
    return state.isAuthenticated && state.isCustomer
  },
  isAdmin(state) {
    return state.isAuthenticated && state.isAdmin
  },
  userId(state) {
    return state.userId
  },
  userProfile(state) {
    return state.userProfile
  }
}

export const actions = {
  authenticate({ commit }, data) {
    commit('SET_IS_GUEST', data.isGuest)
    commit('SET_IS_AUTHENTICATED', data.isAuthenticated)
    commit('SET_IS_CUSTOMER', data.isCustomer)
    commit('SET_IS_ADMIN', data.isAdmin)
    commit('SET_USER_ID', data.userId)
    commit('SET_USER_PROFILE', data.userProfile)
  }
}
